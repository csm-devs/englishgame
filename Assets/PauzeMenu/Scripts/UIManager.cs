﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;
using UnityEngine.Internal;

public class UIManager : MonoBehaviour
{

    public GameObject pausePanel;

    public bool isPaused;
    public Button Play;
    public Image PlayImage;
    public RawImage PlayRawImage;
    public Button Pause;
    public Image PauseImage;
    public RawImage PauseRawImage;
    public float Gamespeed = 1.0f;

    // Use this for initialization
    void Start()
    {
        isPaused = false;
        Play = Play.GetComponent<Button>();
        PlayImage = PlayImage.GetComponent<Image>();
        PlayRawImage = PlayRawImage.GetComponent<RawImage>();
        Pause = Pause.GetComponent<Button>();
        PauseImage = PauseImage.GetComponent<Image>();
        PauseRawImage = PauseRawImage.GetComponent<RawImage>();
        Play.enabled = false;
        PlayImage.enabled = false;
        PlayRawImage.enabled = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPaused)
        {
            PauseGame(true);
        }
        else
        {
            PauseGame(false);
        }

        if (Input.GetButtonDown("Cancel"))
        {
            SwitchPause();
        }
    }

    void PauseGame(bool state)
    {
        if (state)
        {
            Time.timeScale = 0.0f; //Paused
            Play.enabled = true;
            PlayImage.enabled = true;
            PlayRawImage.enabled = true;
            Pause.enabled = false;
            PauseImage.enabled = false;
            PauseRawImage.enabled = false;
        }
        else
        {
            Time.timeScale = Gamespeed; // Unpaused
            Play.enabled = false;
            PlayImage.enabled = false;
            PlayRawImage.enabled = false;
            Pause.enabled = true;
            PauseImage.enabled = true;
            PauseRawImage.enabled = true;
        }
        pausePanel.SetActive(state);
    }

    public void SwitchPause()
    {
        if (isPaused)
        {
            isPaused = false; //Changes the value
            Play.enabled = false;
            PlayImage.enabled = false;
            PlayRawImage.enabled = false;
            Pause.enabled = true;
            PauseImage.enabled = true;
            PauseRawImage.enabled = true;
        }
        else
        {
            isPaused = true;
            Play.enabled = true;
            PlayImage.enabled = true;
            PlayRawImage.enabled = true;
            Pause.enabled = false;
            PauseImage.enabled = false;
            PauseRawImage.enabled = false;
        }
    }
    public void GoToHomeScreen() {
        Application.LoadLevel("HomeScreen");
    }
}