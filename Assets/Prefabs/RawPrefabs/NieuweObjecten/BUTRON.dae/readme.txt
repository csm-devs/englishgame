-- 3dvia.com   --

The zip file BUTRON.dae.zip contains the following files :
- readme.txt
- models/BUTRON.dae
- images/texture9.jpg
- images/texture8.jpg
- images/texture7.jpg
- images/texture6.jpg
- images/texture5.jpg
- images/texture46.jpg
- images/texture45.jpg
- images/texture44.jpg
- images/texture43.jpg
- images/texture42.jpg
- images/texture41.jpg
- images/texture40.jpg
- images/texture4.jpg
- images/texture39.jpg
- images/texture38.jpg
- images/texture37.jpg
- images/texture36.jpg
- images/texture35.jpg
- images/texture34.jpg
- images/texture33.jpg
- images/texture32.jpg
- images/texture31.jpg
- images/texture30.jpg
- images/texture3.jpg
- images/texture29.jpg
- images/texture28.jpg
- images/texture27.jpg
- images/texture26.jpg
- images/texture25.jpg
- images/texture24.jpg
- images/texture23.jpg
- images/texture22.jpg
- images/texture21.jpg
- images/texture20.jpg
- images/texture2.jpg
- images/texture19.jpg
- images/texture18.jpg
- images/texture17.jpg
- images/texture16.jpg
- images/texture15.jpg
- images/texture14.jpg
- images/texture13.jpg
- images/texture12.jpg
- images/texture11.jpg
- images/texture10.jpg
- images/texture1.jpg
- images/texture0.jpg


-- Model information --

Model Name : castle vizcaya butron spain
Author : JIHS on Google Warehouse
Publisher : LizzyD

You can view this model here :
http://www.3dvia.com/content/CE1B33C4D6E8FACC
More models about this author :
http://www.3dvia.com/LizzyD


-- Attached license --

A license is attached to the castle vizcaya butron spain model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution-NonCommercial-ShareAlike 2.5
Detailed license : http://creativecommons.org/licenses/by-nc-sa/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
