-- 3dvia.com   --

The zip file squirrel_guilliutine.3ds.zip contains the following files :
- readme.txt
- squirrel_guilliutine.3ds
- Wood_Pol.jpg
- Wood_Gra.jpg
- Wood_Flo.jpg
- Wood_Bam.jpg
- Wood_B01.jpg
- Squirrel.jpg
- Material.JPG
- GUILROPE.JPG
- GUILMAD1.JPG
- ac3dmat3.jpg


-- Model information --

Model Name : Guillotine - Squirrel-B-Gone 2
Author : Michael Petersen
Publisher : smike

You can view this model here :
http://www.3dvia.com/content/0062A436081A2C3E
More models about this author :
http://www.3dvia.com/smike


-- Attached license --

A license is attached to the Guillotine - Squirrel-B-Gone 2 model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
