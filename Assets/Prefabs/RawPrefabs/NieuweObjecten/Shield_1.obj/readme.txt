-- 3dvia.com   --

The zip file Shield 1.obj.zip contains the following files :
- readme.txt
- Shield 1.obj
- Shield 1.mtl


-- Model information --

Model Name : Shield 1
Author : 
Publisher : Rajash

You can view this model here :
http://www.3dvia.com/content/5798274D5F714355
More models about this author :
http://www.3dvia.com/Rajash


-- Attached license --

A license is attached to the Shield 1 model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
