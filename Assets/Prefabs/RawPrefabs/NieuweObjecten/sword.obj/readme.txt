-- 3dvia.com   --

The zip file sword.obj.zip contains the following files :
- readme.txt
- sword.obj


-- Model information --

Model Name : sword
Author : http://www.virtual-lands-3d.com/model-tags.html?start=0&tags=Free+3d+models
Publisher : ModelMia

You can view this model here :
http://www.3dvia.com/content/3BCDDE3103152739
More models about this author :
http://www.3dvia.com/ModelMia


-- Attached license --

A license is attached to the sword model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
