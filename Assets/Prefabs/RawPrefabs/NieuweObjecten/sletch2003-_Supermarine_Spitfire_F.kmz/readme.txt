-- 3dvia.com   --

The zip file sletch2003- Supermarine_Spitfire_F.kmz.zip contains the following files :
- readme.txt
- sletch2003- Supermarine_Spitfire_F.kmz


-- Model information --

Model Name : Supermarine Spitfire
Author : sletch2003
Publisher : billybobsue

You can view this model here :
http://www.3dvia.com/content/A573099BADBF91A3
More models about this author :
http://www.3dvia.com/billybobsue


-- Attached license --

A license is attached to the Supermarine Spitfire model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
