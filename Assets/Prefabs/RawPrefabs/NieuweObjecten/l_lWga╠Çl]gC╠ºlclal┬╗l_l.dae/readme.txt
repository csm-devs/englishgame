-- 3dvia.com   --

The zip file ╬┐╬╗╧à╬╝╧Ç╬╣╬╡╬»╬┐╬╜.dae.zip contains the following files :
- readme.txt
- models/╬┐╬╗╧à╬╝╧Ç╬╣╬╡╬»╬┐╬╜.dae
- images/__Logs_.jpg
- images/__Chalk_noCulling.jpg
- images/__Chalk_1.jpg
- images/Tile_Ceramic_Natural.jpg
- images/Stone_Vein_GraynoCulling.jpg
- images/Stone_Masonry_MultinoCulling.jpg
- images/Stone_Brushed_Khaki.jpg
- images/Roofing_Tile_SpanishnoCulling.jpg
- images/Material4noCulling.JPG
- images/Material2noCulling.JPG
- images/Material1_1.JPG
- images/Material1noCulling.JPG
- images/Brick_Rough_Tan1noCulling.jpg
- images/Brick_Rough_Tan.jpg


-- Model information --

Model Name : Temple of Olympian Zeus, Athens
Author : Dimitris Tsalkanis
Publisher : dimitsal

You can view this model here :
http://www.3dvia.com/content/33F2D6293B0D1F31
More models about this author :
http://www.3dvia.com/dimitsal


-- Attached license --

A license is attached to the Temple of Olympian Zeus, Athens model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution-NonCommercial-ShareAlike 2.5
Detailed license : http://creativecommons.org/licenses/by-nc-sa/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
