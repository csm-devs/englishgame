This helmet was modeled by Bjorn Lovoll.  It are intended to be used in the creation of Civ3 user art such as units, leader heads or other civ3 modifcation files.  But, permission is granted to use them in any non-commercial work.  If you want to use them in a commercial project, we can make arrangements for you to pay for the rights to do so.

Permission is also granted to distribute them in any collection, or other distribution method so long as none of the contents of the orignal package including this notice is modified or ommitted and that any such redistribution be completely free of any and all charges, access fees, distribution costs, shipping costs, etc.

There is no guarentee expressed nor implied as to their suitability to any task.

------------
To use place in a runtime such as:
C:\Program Files\Curious Labs\poser7\Runtime\Libraries\Props\bjorn\
------------

In this case (to make things a little easier on everyone), the zip file contains a default tree structure.
\Runtime\Readme\
\Runtime\Textures\Bjorn\
\Runtime\Libraries\Props\Bjorn\
\Runtime\Geometries\Bjorn\




There are two version of the helmet.  1 with a open chinstrap, the with it fastened.

The OBJ is imbedded in the Poser pp2 file.
The geometries folder does contain the source file (in obj and 3ds format)

Questions? Contact me.


bjorn@lovoll.net