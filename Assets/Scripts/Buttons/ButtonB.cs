﻿using UnityEngine;
using System.Collections;

public class ButtonB : MonoBehaviour {
	ItemScript itemscript;
	ObjectBeforePlayer PlayerA;
	bool itemHits = false;
	// Use this for initialization
	void Start () {
		PlayerA = GameObject.Find ("Player").GetComponent<ObjectBeforePlayer> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		if (itemHits == true) {
			itemscript = PlayerA.itemHit.GetComponent<ItemScript> ();
		}
	}
	public void returnB() {
		itemscript.Goed_B();
	}
	public void ItemHitted() {
		itemHits = true;
	}
}
