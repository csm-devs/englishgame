﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float speed = 2f;
	public float rotateSpeed;
	public float Roteer;
	Vector3 position;
	public bool forwardBool = false;
	public bool backwardBool = false;
	public bool rotateLeftBool = false;
	public bool rotateRightBool = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		rotateSpeed = Roteer * Time.deltaTime;
		position = transform.position;
		if (forwardBool == true) {
			position += transform.forward * speed * Time.deltaTime;
		}
		if (backwardBool == true) {
			position -= transform.forward * speed * Time.deltaTime;
		}
		if (rotateLeftBool == true) {
			transform.Rotate(0,-rotateSpeed,0);
		}
		if (rotateRightBool == true) {
			transform.Rotate(0,rotateSpeed,0);
		}
		transform.position = position;

	}
	public void forward() {
		forwardBool = true;
	}
	public void notForward() {
		forwardBool = false;
	}
	public void back() {
		backwardBool = true;
	}
	public void notBack() {
		backwardBool = false;
	}
	public void right() {
		rotateRightBool = true;
	}
	public void notRight() {
		rotateRightBool = false;
	}
	public void left() {
		rotateLeftBool = true;
	}
	public void notLeft() {
		rotateLeftBool = false;
	}











}
