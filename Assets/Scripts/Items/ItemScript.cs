﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ItemScript : MonoBehaviour {
	Canvas Questions;

	public string Vragen;
	public string Antwoord1;
	public string Antwoord2;
	public string Antwoord3;
	public string Antwoord4;
	Text Antwoord_A_Text;
	Text Antwoord_B_Text;
	Text Antwoord_C_Text;
	Text Antwoord_D_Text;
	bool EenBezet = false;
	bool TweeBezet = false;
	bool DrieBezet = false;
	bool VierBezet = false;
	public bool GoedA;
	public bool GoedB;
	public bool GoedC;
	public bool GoedD;
	Button A;
	Button B;
	Button C;
	Button D;
	int Score;
	public bool x = false;
	public bool y = false;
	public bool z = false;

	// Use this for initialization
	void Start () {
		A = GameObject.FindWithTag ("A").GetComponent<Button> ();
		B = GameObject.FindWithTag ("B").GetComponent<Button> ();
		C = GameObject.FindWithTag ("C").GetComponent<Button> ();
		D = GameObject.FindWithTag ("D").GetComponent<Button> ();

		Questions = GameObject.FindWithTag ("VragenCanvas").GetComponent<Canvas> ();
	

	}
	
	// Update is called once per frame
	void Update () {
		if (x == true) {
			transform.Rotate (1, 0, 0);
		}
		if (y == true) {
			transform.Rotate (0, 1, 0);
		}
		if (z == true) {
			transform.Rotate (0, 0, 1);
		}
		Score = PlayerPrefs.GetInt ("Score");

	}

	public void Vraag() {
		GameObject.FindWithTag ("Vraag").GetComponent<Text> ().text = Vragen;
		GameObject.FindWithTag ("TextA").GetComponent<Text> ().text = Antwoord1;
		GameObject.FindWithTag ("TextB").GetComponent<Text> ().text = Antwoord2;
		GameObject.FindWithTag ("TextC").GetComponent<Text> ().text = Antwoord3;
		GameObject.FindWithTag ("TextD").GetComponent<Text> ().text = Antwoord4;
		Questions.enabled = true;
	}
	public void Goed_A() {
		if(GoedA == true) {
			print ("Goed");
			Score += 1;
			Questions.enabled = false;
			PlayerPrefs.SetInt ("Score", Score);
			Destroy(this.gameObject);
		} 
		if(GoedA == false) {
			print ("fout");
			Score -= 1;
			Questions.enabled = false;
			PlayerPrefs.SetInt ("Score", Score);
			Destroy(this.gameObject);
		}

	}
	public void Goed_B() {
		if(GoedB == true) {
			print ("Goed");
			Score += 1;
			Questions.enabled = false;
			PlayerPrefs.SetInt ("Score", Score);
			Destroy(this.gameObject);
		} 
		if(GoedB == false) {
			print ("fout");
			Score -= 1;
			Questions.enabled = false;
			PlayerPrefs.SetInt ("Score", Score);
			Destroy(this.gameObject);
		}
		
	}
	public void Goed_C() {
		if(GoedC == true) {
			print ("Goed");
			Score += 1;
			Questions.enabled = false;
			PlayerPrefs.SetInt ("Score", Score);
			Destroy(this.gameObject);
		} 
		if(GoedC == false) {
			print ("fout");
			Score -= 1;
			Questions.enabled = false;
			PlayerPrefs.SetInt ("Score", Score);
			Destroy(this.gameObject);
		}
		
	}
	public void Goed_D() {
		if(GoedD == true) {
			print ("Goed");
			Score += 1;
			Questions.enabled = false;
			PlayerPrefs.SetInt ("Score", Score);
			Destroy(this.gameObject);
		} 
		if(GoedD == false) {
			print ("fout");
			Score -= 1;
			Questions.enabled = false;
			PlayerPrefs.SetInt ("Score", Score);
			Destroy(this.gameObject);
		}
		
	}

}



