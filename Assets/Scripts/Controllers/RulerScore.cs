﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class RulerScore : MonoBehaviour {
	Text score;
    public int scoreOmDitLevelTeHalen;
    public Canvas gehaaldScherm;
    private int scoreForTest;
	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt ("Score", 0);
		score = GameObject.Find ("Score").GetComponent<Text> ();
        gehaaldScherm = gehaaldScherm.GetComponent<Canvas>();
        gehaaldScherm.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		score.text ="Punten: " + PlayerPrefs.GetInt ("Score").ToString ();
        scoreForTest = PlayerPrefs.GetInt("Score");

        if (scoreForTest == scoreOmDitLevelTeHalen) {
            gehaaldScherm.enabled = true;
        }
	}
    public void GoToGame() {
        Application.LoadLevel("Portals");
    }
}
