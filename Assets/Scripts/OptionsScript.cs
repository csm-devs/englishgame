﻿using UnityEngine;
using System.Collections;

public class OptionsScript : MonoBehaviour {
    public Canvas MoreInfo;

    void Start() {
        MoreInfo = MoreInfo.GetComponent<Canvas>();
        MoreInfo.enabled = false;
    }

    public void Fastest() {
        QualitySettings.currentLevel = QualityLevel.Fastest;
        Debug.Log("Fastest");
    }
    public void Fast() {
        QualitySettings.currentLevel = QualityLevel.Fast;
        Debug.Log("Fast");
    }
    public void Simple() {
        QualitySettings.currentLevel = QualityLevel.Simple;
        Debug.Log("Simple");
    }

    public void Good() {
        QualitySettings.currentLevel = QualityLevel.Good;
        Debug.Log("Good");
    }
    public void Beautiful() {
        QualitySettings.currentLevel = QualityLevel.Beautiful;
        Debug.Log("Beautiful");
    }
    public void Fantastic() {
        QualitySettings.currentLevel = QualityLevel.Fantastic;
        Debug.Log("Fantastic");
    }
    public void ShowMoreInfo() {
        MoreInfo.enabled = true;
    }
    public void HideMoreInfo() {
        MoreInfo.enabled = false;
    }
    public void GoToHome() {
        Application.LoadLevel("HomeScreen");
    }
}


