# How to play the game
There two ways you can play the game.
First there's the simple version. You can just go to [releases](https://github.com/CSMdevs/EnglishGame/releases) and download the version available for windows.
If you want to play on Mac, Linux or any other OS, you'll have to clone the [source code](https://bitbucket.org/csm-devs/englishgame/src/master/).
````
git clone git@bitbucket.org:csm-devs/englishgame.git
````
When finished,  open the game with Unity 5.6.0 or up.
````
Open Unity
````
Open the Build Settings
````
File > Build Settings......
````
Select the OS that you want to build for and click Build&Run
````
Build And Run
````

LICENSE
````
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>
````
